from django.db import models
from django.contrib.auth.models import User


class Group(models.Model):
    gr_name = models.CharField(max_length=20, unique=True)
    gr_create_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.gr_name}'


class Member(models.Model):
    mr_group = models.ForeignKey(Group, on_delete=models.CASCADE)
    mr_user = models.ForeignKey(User, on_delete=models.CASCADE)
    mr_name = models.CharField(max_length=50)
    mr_register_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.mr_group.gr_name}_{self.mr_user.username}_{self.mr_name} ({self.id})'


class Message(models.Model):
    ms_member = models.ForeignKey(Member, on_delete=models.CASCADE)
    ms_text = models.CharField(max_length=200)
    ms_create_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.ms_member.mr_group.gr_name}_{self.ms_member.mr_name} ({self.ms_create_date}): {self.ms_text}'
