from django.shortcuts import render
from django.utils.safestring import mark_safe
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect, Http404

from chat.models import Group, Member, Message

import json


def index(request):
    return render(request, 'index.html', {'group_list': Group.objects.all(),
                                          'member_list': Member.objects.all(),
                                          'message_list': Message.objects.all(),
                                          'user': request.user})


@login_required(login_url='login')
def room(request, room_name):
    group = Group.objects.filter(gr_name=room_name).first()
    if not group:
        raise Http404
    member = group.member_set.filter(mr_user=request.user).first()
    if not member:
        member = group.member_set.create(mr_user=request.user, mr_group=group, mr_name=request.user.username)
    message_list = Message.objects.filter(ms_member__mr_group=group).order_by('ms_create_date').all()
    return render(request, 'chats_new.html', {
        'room_name_json': mark_safe(json.dumps(room_name)),
        'member_list': group.member_set.all(),
        'message_list': message_list,
        'group_list': Group.objects.all(),
        'user': request.user,
        'member': member
    })


def auth(request):
    if request.method == 'POST':
        next = request.GET.get('next', '/')
        data = request.POST.copy()
        username = data.get('username')
        password = data.get('password')
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            return HttpResponseRedirect(next)
        else:
            return render(request, 'login-simple.html')
    else:
        return render(request, 'login-simple.html')
