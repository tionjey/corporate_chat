# Generated by Django 2.2.1 on 2019-05-08 13:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('gr_name', models.CharField(max_length=20)),
                ('gr_create_date', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Member',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mr_name', models.CharField(max_length=50)),
                ('mr_register_date', models.DateTimeField()),
                ('mr_group', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='chat.Group')),
            ],
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ms_text', models.CharField(max_length=200)),
                ('mr_create_date', models.DateTimeField()),
                ('ms_member', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='chat.Member')),
            ],
        ),
    ]
