from django.contrib import admin

# Register your models here.
from chat.models import Group, Member, Message

admin.site.register(Group)
admin.site.register(Member)
admin.site.register(Message)
