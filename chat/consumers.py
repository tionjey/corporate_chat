import json
import logging

from channels.generic.websocket import AsyncWebsocketConsumer
from channels.db import database_sync_to_async

from chat.models import Group, Member, Message

logger = logging.getLogger('chatroom')


class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name
        self.user = self.scope['user']
        logger.info(f'connect new user to group: {self.user} => {self.room_group_name}')
        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )
        await self.accept()
        await self.add_group()
        # print('create connect')

    async def disconnect(self, close_code):
        # Leave room group
        logger.info(f'leave group: {self.user} => {self.room_group_name}')
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        member = await self.get_member()
        # Send message to room group
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message,
                'member': member.mr_name,
                'member_id': member.id
            }
        )
        await self.save_message(member, message)

    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']
        member = event['member']
        member_id = event['member_id']
        logger.info(f'get message from user: {self.user} => {self.room_group_name}')
        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message,
            'member': member,
            'member_id': member_id
        }))

    @database_sync_to_async
    def add_group(self):
        # group = Group(gr_name=self.room_name)
        group = Group.objects.filter(gr_name=self.room_name).first()
        if not group:
            logger.critical(f'create new group: {self.user} => {self.room_group_name}')
            group = Group(gr_name=self.room_name)
            group.save()
        # print('save group')
        return group

    @database_sync_to_async
    def get_member(self):
        group = Group.objects.filter(gr_name=self.room_name).first()
        member = Member.objects.filter(mr_user=self.user, mr_group=group).first()
        if not member:
            logger.critical(f'create new member for group: {self.user} => {self.room_group_name}')
            member = Member(mr_user=self.user, mr_group=group, mr_name=self.user.username)
            member.save()
        # print('save member')
        return member

    @database_sync_to_async
    def save_message(self, member, text):
        logger.debug(f'save message into database: {self.user} => {self.room_group_name}')
        message = Message(ms_member=member, ms_text=text)
        message.save()
        # print('save message')
