from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'login$', views.auth, name='login'),
    url(r'room/(?P<room_name>[^/]+)/$', views.room, name='room'),
]
