```sh
python3 -m venv venv
```


```sh
venv/bin/pip install -r requirements.txt
```


```sh
sudo docker run -p 6379:6379 -d redis:2.8
```


```sh
venv/bin/python manage.py migrate
```


```sh
venv/bin/python manage.py runserver
```